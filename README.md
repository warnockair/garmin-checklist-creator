# README #

Garmin's newer systems such as the GTN and the X series allow for integrated checklists.  This project is designed for a web based editor for the ACE file format.
This will allow users to upload, edit or create new checklists.
It would allow sharing of these checklist as jumpstarts or completes for make an model of aircraft.

### What is this repository for? ###

* Garmin Checklist Editor
* Next Step is to figure out the formats of the file
* Document what each offset means
* create a markdown model
* code up and example to start testing

### Code used for this project ###
* Project is React with YII as a backend
* Rest based services for the data exchange
* Firebase for the backend datastore.


### Currently know format example ###
```
 1 ðððð^@^A^@^@
 2 GARMIN CHECKLIST PN XXX-XXXXX-XX
 3 Aircraft Make and Model
 4 Aircraft specific identification
 5 Manufacturer Identification
 6 Copyright Information
 7 <0NORMAL OPERATIONS
 8 (0BEFORE STARTING
 9 r0Seats, Belts ~CHECK and ADJUST
 10 r0Brakes~ON
 11 r0Fuel Selector~FULLEST TANK
 12 r0Circuit Breakers~CHECK IN
 13 r0Radios / Electrical Switches~OFF
 14 r0Alternate Air~OFF
 15 r0Master Switch~ON
 16 r0Landing Gear Lights~3 GREEN
 17 r0Auto Pilot~OFF
 18 )
 19 (0START ENGINE COLD
 20 r0Mixture~RICH
 21 r0Throttle~FULL FOWARD
 22 r0Propeller~FULL FORWARD
 23 r0Aux Fuel Pump~OFF
 24 r0Primer~3-5 Seconds
 25 r0Throttle~RETARD
 26 r0Propeller Area~CLEAR
 27 r0Ignition Switch~START
 28 r0Throttle~1,000 RPM
 29 r0Oil Temp~GREEN
 30 r0Oil Pressure~GREEN
 31 r0Alternator Switch~ON
 32 r0Engine Instruments~CHECK
 33 r0Fuel Gauge~CHECK
 34 r0Avionics Master~ON
 35 r0Beacon / Anti-Collision Lights~ON
 36 r0Trasponder~STANDBY
 37 )
```